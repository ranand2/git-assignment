# Assignment #1

Learn git from: https://git-scm.com/docs/gittutorial

Before beginning with the assignment make sure you registered on http://gitlab.com/

If you have not registered yet, please create an account on http://gitlab.com/


1. Fork this repository on gitlab account: https://gitlab.com/symb-assessment/startbootstrap-bare
2. Add tag to the repository as version *v0.0.1*
3. Create new branch 'contact_us', and checkout that branch
4. Now create new file 'contact_us.html' in the repository, with content
    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <title>Contact US</title>
    </head>
      <body>
        <h1>Contact Us</h1>
      </body>
    </html>
    ``` 
5. Commit the code and push it to the repository
6. Now goto 'master' branch create a new branch 'media' and checkout 
7. create new directory 'images' in the code and add 2 image files with name:
    1. logo.png
    2. profile.jpg
8. Push the code and raise a merge request to merge 'media' to master
9. Approve the merge request created in step 8.
10. Tag the 'master' branch with version *v0.0.2*
11. Raise a merge request for the **contact_us** branch to merge it to master
12. Tag the 'master' branch with version *v0.0.3*
13. Push all the changes to the remote repository
